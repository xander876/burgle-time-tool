use std::{
    fmt::Debug,
    fs,
    io::BufRead,
    ops::{Add, Sub},
};

#[derive(Default, Copy, Clone)]
struct HourMin {
    hour: u8,
    min: u8,
}

#[derive(Default)]
struct Date {
    day: u8,
    month: u8,
    year: u8,
}

fn main() {
    let f = fs::read("/home/alex/Work/notes/burglebrothersvr_time.md").unwrap();

    let mut in_times = false;
    let mut any_time = false;
    let mut total_time = HourMin::default();
    let mut total_of_date = HourMin::default();
    let mut date = Date::default();

    f.lines().for_each(|line| {
        let line = line.unwrap();
        if line.starts_with("##### Times") {
            in_times = true;
        }
        if line.starts_with("### ") {
            let s = line.split_whitespace();
            let date_str = s.last().unwrap();
            if !date_str.starts_with("{") {
                if total_of_date.hour > 0 || total_of_date.min > 0 {
                    println!("On {:?} with a total of {:?} and in minutes: {}", date, total_of_date, total_of_date.minutes());
                }
                date = Date::from(date_str);
                total_of_date = HourMin::default();
            }
        }
        if in_times && line.starts_with("*") && !line.starts_with("* {") {
            any_time = true;
            let mut split = line.split_whitespace();
            let start_time = HourMin::from(split.nth(1).unwrap());
            let end_time = HourMin::from(split.nth(1).unwrap());
            let line_time = end_time - start_time;
            //println!("{:?}", line_time);
            total_of_date = total_of_date + line_time;
            total_time = total_time + line_time;
        } else if any_time && line.trim().len() <= 0 {
            any_time = false;
            in_times = false;
        }
    });
    println!("On {:?} with a total of {:?} and in minutes: {}", date, total_of_date, total_of_date.minutes());
    println!("\nTotal Time Is {:?} and in minutes: {}", total_time, total_time.minutes());
}

impl From<&str> for HourMin {
    fn from(value: &str) -> Self {
        let mut s = value.split(":");
        let h: u8 = s.nth(0).unwrap().parse().unwrap();
        let m: u8 = s.nth(0).unwrap().parse().unwrap();

        HourMin { hour: h, min: m }
    }
}

impl From<&str> for Date {
    fn from(value: &str) -> Self {
        let mut s = value.split("/");
        Date {
            month: s.nth(0).unwrap().parse().unwrap(),
            day: s.nth(0).unwrap().parse().unwrap(),
            year: s.nth(0).unwrap().parse().unwrap(),
        }
    }
}

impl HourMin {
    fn reduce(self) -> HourMin {
        HourMin {
            hour: self.hour + self.min / 60,
            min: self.min % 60,
        }
    }
    fn minutes(&self) -> usize {
        let h = self.hour as usize;
        let m = self.min as usize;
        h * 60 + m
    }
}

impl Add for HourMin {
    type Output = HourMin;
    fn add(self, rhs: Self) -> Self::Output {
        let hm = HourMin {
            hour: self.hour + rhs.hour,
            min: self.min + rhs.min,
        };
        hm.reduce()
    }
}

impl Sub for HourMin {
    type Output = HourMin;
    fn sub(self, rhs: Self) -> Self::Output {
        let mut hm = HourMin::default();
        hm.hour = self.hour;
        if hm.hour < rhs.hour {
            hm.hour += 24;
        }
        hm.min = self.min;
        if hm.min < rhs.min {
            hm.hour -= 1;
            hm.min += 60;
        }
        hm.hour -= rhs.hour;
        hm.min -= rhs.min;
        hm.reduce()
    }
}

impl Debug for Date {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}/{}/{}", self.month, self.day, self.year)
    }
}

impl Debug for HourMin {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}hrs {}mins", self.hour, self.min)
    }
}
